#!/bin/python
import re
from pandas import *
import pandas as pd
from ggplot import *
import os.path


class HpccOut:
    def __init__(self, filePath):
        self.output=self.getSeries(filePath)

    def getName(self, filePath):
        return filePath

    def getInput(self, filePath):
        #main var:
        inputName=['Ns', 'NBs', 'Ps', 'Qs']
        inputValue=[]
        #find the dir name
        dn = filePath[:filePath.find('parsed-hpccout')]
        #set path of hpccinf.txt
        pathHpccinf = dn + 'hpccinf.txt'
        if not os.path.isfile(pathHpccinf):
            print('Hpcc input file', pathHpccinf, 'not exsits, using the default value:')
            print('Number of Nodes: -1, Ps: -1, Qs: -1')
            inputName=['Ns', 'NBs', 'Ps', 'Qs', 'NumOfCores']
            inputValue = [-1, -1, -1, -1, -1]
        else:
            # get the content of the input file
            fHpccinf = open(pathHpccinf).read()
            for i in inputName:
                inputValue.append(self.getSectionValue(fHpccinf, i))
            pIndex = inputName.index('Ps')
            qIndex = inputName.index('Qs')
            numOfNodes = inputValue[pIndex] * inputValue[qIndex]
            inputName.append('NumOfCores')
            inputValue.append(numOfNodes)
        
        #append the number of nodes to lists
        inputName.append('NodesNum')
        inputValue.append(self.getNodes(dn))
        return inputName, inputValue

    def getNodes(self, dirname):
        try:
            f = open(dirname+'submit.pbs')
        except IOError:
            print('can not open file', dirname, 'submit.pbs')
            return -1
        #find the node number
        fileContent = f.read()
        index1 = fileContent.find('=')
        index2 = index1 + fileContent[index1:].find('\n')
        index3 = index1 + fileContent[index1:].find(':')
        startIndex = index1
        if index3 > 0:
            endIndex = index3;
        else:
            endIndex = index2;
        try:
            # getting the nodes value
            # +1 is used for omit the '=' symbol
            nodesNum = int(fileContent[startIndex + 1:endIndex])
        except ValueError:
            print('converting int error')
            print(fileContent[index1:index2])
            nodesNum = -1
        return nodesNum

    # get the corresponding value of a section from hpccintf file
    def getSectionValue(self,fileContent, secName):
        if secName == 'NBs':
            index1 = fileContent.rfind(secName)
        else:
            index1 = fileContent.find(secName)
        index2 = fileContent[:index1].rfind('\n')

        try:
            value = int(fileContent[index2:index1])
        except ValueError:
            value = -1
        return value


    def getSeries(self, filePath):
        hpccFile = open(filePath, 'r')
        fileContent = hpccFile.read()
        fileLines = fileContent.split('\n')[:-1]
        #print(fileLines)
        names, values = self.getInput(filePath)
        for i in fileLines:
            isp = i.split(':')
            names.append(isp[0])
            values.append(float(isp[1]))
        #build data frame
        names.append('Path')
        values.append(self.getName(filePath))
        return Series(values, index=names)

class DataCollection:
    def __init__(self,fp):
        self.filePaths = fp
        self.hpccout=[] # hpccout file path
        self.makeDataFrame()

    def makeDataFrame(self):
        self.hpccout = [ HpccOut(i) for i in self.filePaths ]
        d = {}
        for i in range(len(self.hpccout)):
            d[i] = self.hpccout[i].output

        self.df = DataFrame(d)


class FilePaths:
    def __init__(self, filePath):
        f = open(filePath, 'r')
        self.pathList=[]
        for i in f.read().split('\n'):
            if os.path.isfile(i):
                self.pathList.append(i)
        print('total number of :', len(self.pathList), 'output files added')


