#!/bin/python
from ggplot import *
from pandas import * 
import pandas as pd
from hpccParser import HpccOut, DataCollection, FilePaths

filePaths= FilePaths('./paths-hpccout')
#a = HpccOut(filePaths.pathList[1])

dc = DataCollection(filePaths.pathList)
a = dc.df.T
run2 = a[a['Path'].str.contains('run2')]
ppl=run2.filter(regex='PingPongLa')
ppl.columns=['Avg', 'Max', 'Min']
ppl['NumOfCores']=run2['NumOfCores']
pplL=melt(ppl, id_vars='NumOfCores', value_name='PingPong Latency')
p = ggplot(pplL, aes('NumOfCores', 'PingPong Latency', colour='variable'))+ geom_point() + geom_line()
