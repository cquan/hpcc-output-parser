#!/bin/python
from ggplot import *
from pandas import *
import pandas as pd
from hpccParser import HpccOut, DataCollection, FilePaths


def plotPingPong(a, runNum, lob):
    # runNum: '2', '1'; lob: 'B', 'L'
    if lob == 'B':
        texts = 'Bandwidth'
    elif lob == 'L':
        texts = 'Latency'

    run2 = a[a['Path'].str.contains('run'+runNum)]
    ppl=run2.filter(regex='PingPong'+lob)
    ppl.columns=['Avg', 'Max', 'Min']
    ppl['Number Of Cores']=run2['NumOfCores']
    valName = 'PingPong ' + texts
    pplL=melt(ppl, id_vars='Number Of Cores', value_name=valName, var_name='Types')
    p = ggplot(pplL, aes('Number Of Cores', valName, colour='Types'))+ geom_point() + geom_line()
    return p;

def plotPtrans(a,i):
    # i is either '3' or '4'
    l=a[a['Path'].str.contains('run'+i+'.*numa-affinity')]
    p = ggplot(l, aes('NumOfCores', 'PTRANS_GBs',label='NodesNum')) + geom_point() + geom_line() + xlab('Number of Cores') + ylab('Total Bandwidth (GB/s)')  + geom_text(hjust=-0.5, vjust=0.5)

    return p;

def plotDecompNBs(a):
    decomp=a[a['Path'].str.contains('numOfCoresPerProcessor.*\d/par.*')]
    # decomp=a[a['Path'].str.contains('numOfCoresPerProcessor/4.*/\d{1,2}/par.*')]
    decomp = decomp.drop([3, 4, 6, 13, 14, 16]) #have some extra runs need to eliminate
    nbs=DataFrame()
    index=[0,1,2,3,4,5,6]

    t=decomp[decomp['NBs']==50]['PTRANS_GBs']
    t.index=index
    nbs['NBs50']=t
    t=decomp[decomp['NBs']==100]['PTRANS_GBs']
    t.index=index
    nbs['NBs100']=t
    t=decomp[decomp['NBs']==500]['PTRANS_GBs']
    t.index=index
    nbs['NBs500']=t
    nbs['CoresPerProcessor']=[1,12,2,3,4,6,0]
    
    #remove the last row of nbs
    nbs = nbs.ix[:5]
    
    nbsl=melt(nbs, id_vars='CoresPerProcessor', value_name='PTRANS_GBs', var_name='NB Size')
    p = ggplot(nbsl, aes('CoresPerProcessor', 'PTRANS_GBs', colour='NB Size')) + geom_point() + geom_line() + xlab('Cores per Processor') + ylab('Total bandwidth (GB/s)')
    return p;

def plotDecompProbSize(a):
    ps=a[a['Path'].str.contains('problemSize')][:-1]
    #typelist=['1x24', '2x12', '4x6', '6x4']
    typelist=['1x24', '2x12', '4x6']
    index = [i for i in range(9)]
    problemSize=[100, 1000, 10000, 200, 2000, 20000, 500, 5000, 50000]
    psl= DataFrame()
    for i in typelist:
        tmp=ps[ps['Path'].str.contains(i)]
        tmp.index = index
        psl[i] = tmp['PTRANS_GBs']

    psl['ProblemSize']= problemSize;
    psll=melt(psl, id_vars='ProblemSize', value_name='PTRANS_GBs', var_name='Decomposition')
    p=ggplot(psll, aes('ProblemSize', 'PTRANS_GBs', colour='Decomposition')) + geom_point() + geom_line() + xlab('Problem Size') + ylab('Total bandwidth (GB/s)') + scale_x_log10() + scale_x_continuous(limits=(80,1E5))
    #p=qplot('ProblemSize', 'PTRANS_GBs', data=psll, log='x', color='Decomposition') + geom_point() + geom_line()
    return p

def hplVsAccuRandomRing(a,i):
    run3=a[a.Path.str.contains('run'+i+'.*numa-affinity.*')]
    accumuRun3=DataFrame()
    accumuRun3['Accumulated RandRing Bandwidth GBytes']=run3['RandomlyOrderedRingBandwidth_GBytes'] * run3['NumOfCores']
    accumuRun3['Linpack TFlop/s'] = run3['HPL_Tflops']
    accumuRun3['NumOfNodes'] = run3['NodesNum']
    p = ggplot(accumuRun3, aes('Linpack TFlop/s', 'Accumulated RandRing Bandwidth GBytes', label='NumOfNodes')) + geom_point() + geom_line(color='steelblue') + geom_text(hjust=0.15, vjust=0.1)
    return p

def bwthVsCorePerProcessor(a):
    sumData=a[a['Path'].str.contains('numOfCoresPerProcessor.*NB.*/\d{1,2}?/p')]
    plotData=DataFrame()
    index=[i for i in range(6)]

    tmp=sumData[sumData['Path'].str.contains('NB50/')]['PTRANS_GBs']
    tmp.index=index
    plotData['NBs 50']=tmp

    tmp=sumData[sumData['Path'].str.contains('NB500/')]['PTRANS_GBs']
    tmp.index=index
    plotData['NBs 500']=tmp

    tmp=sumData[sumData['Path'].str.contains('NB100/')]['PTRANS_GBs']
    tmp.index=index
    plotData['NBs 100']=tmp

    coresPerProcessor=[1,12,2,3,4,6]
    plotData['Core Per Processor']=coresPerProcessor
    plotDataL=melt(plotData, id_vars='Core Per Processor', value_name='PTRANS_GBs', var_name='NB Size')
    p=ggplot(plotDataL, aes('Core Per Processor', 'PTRANS_GBs', colour='NB Size')) + geom_point() + geom_line()
    return p

def bwthVsDecomposition(a):
    sumD=a[a['Path'].str.contains('problemSize/\d.*')]
    probSize=[100,1000,10000, 200,2000,20000,500,5000,50000]
    index=[i for i in range(9)]
    plotD=DataFrame()

    for i in ['1x24', '2x12', '4x6', '6x4']:
        tmp=sumD[sumD['Path'].str.contains(i)]['PTRANS_GBs']
        tmp.index=index
        plotD[i]=tmp

    plotD['Problem Size']=probSize
    plotDL=melt(plotD, id_vars='Problem Size', value_name='PTRANS_GBs', var_name='Decomposition')
    #normal scale
    pn=ggplot(plotDL, aes('Problem Size', 'PTRANS_GBs', colour='Decomposition')) + geom_point() + geom_line()
    pl=qplot('Problem Size', 'PTRANS_GBs', data=plotDL, color='Decomposition', log=u'x') + geom_point() + geom_line() + scale_x_log10(10) + scale_x_continuous(limits=(50,1e5))
    return pn, pl

pd.set_option('max_colwidth', 100)
#filePaths= FilePaths('./paths-hpccout')
filePaths= FilePaths('./filePaths-ptrans-run5.txt')
dc = DataCollection(filePaths.pathList)
a = dc.df.T
